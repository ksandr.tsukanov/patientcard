﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PatientCard.Helpers
{
    public class ValidateDateAttribute : ValidationAttribute
    {
        private readonly DateTime _maxValue = DateTime.Now;

        public override bool IsValid(object value)
        {
            DateTime val = (DateTime)value;
            return val <= _maxValue;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(ErrorMessage, _maxValue);
        }
    }
}