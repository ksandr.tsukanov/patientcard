﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using PatientCard.Models.DataModels;

namespace PatientCard.Helpers
{
    public class RepositoryHelper
    {
        private static PatientCardDataContext _context;

        private static PatientCardDataContext Context => _context ?? (_context = new PatientCardDataContext());

        public static IQueryable<TEntity> Query<TEntity>()
                    where TEntity : class
        {
            var context = Context;

            return context.Set<TEntity>();
        }
        public static void Insert<TEntity>(TEntity entity) where TEntity : class
        {
            var context = Context;

            context.Entry(entity).State = EntityState.Added;
            context.SaveChanges();
        }

        /// <summary>
        /// Запись нескольких полей в БД
        /// </summary>
        public static void Inserts<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            var context = Context;

            // Отключаем отслеживание и проверку изменений для оптимизации вставки множества полей
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;

            foreach (var entity in entities)
                context.Entry(entity).State = EntityState.Added;
            context.SaveChanges();

            context.Configuration.AutoDetectChangesEnabled = true;
            context.Configuration.ValidateOnSaveEnabled = true;
        }

        public static void Update<TEntity>(TEntity entity)
            where TEntity : class
        {
            var context = Context;

            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
        }

        public static void Delete<TEntity>(TEntity entity)
            where TEntity : class
        {
            var context = Context;

            context.Entry(entity).State = EntityState.Deleted;
            context.SaveChanges();
        }
    }
}
