﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PatientCard.Helpers;
using PatientCard.Models;
using PatientCard.Models.DataModels;

namespace PatientCard.Controllers
{
    public class PatientCardController : Controller
    {
        #region MainPage

        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region Patients

        public ActionResult Patient()
        {
            return View(new List<PatientViewModel>());
        }

        [HttpPost]
        public ActionResult PatientDetails(PatientViewModel model)
        {
            return PartialView(model);
        }

        #endregion

        #region VisitHistory

        public ActionResult VisitHistory(long patientId)
        {
            ViewBag.PatientId = patientId;
            return View(new List<VisitHistoryViewModel>());
        }


        [HttpPost]
        public ActionResult VisitInfo(VisitHistoryViewModel model)
        {
            return PartialView(model);
        }

        #endregion
    }
}