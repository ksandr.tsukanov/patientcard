﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using PatientCard.Helpers;
using PatientCard.Models;
using PatientCard.Models.DataModels;

namespace PatientCard.Controllers
{
    /// <summary>
    /// Информация о пациентах
    /// </summary>
    public class PatientsController : ApiController
    {
        /// <summary>
        /// Получить список всех пациентов
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public List<PatientViewModel> Patient()
        {
            var list = RepositoryHelper.Query<Patient>().ToList();
            var model = ViewModelBase.ToViewModel<Patient, PatientViewModel>(list);
            return model;
        }

        /// <summary>
        /// Получить данные пациента по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор пациента</param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public PatientViewModel Patient(long id)
        {
            var patient = id > 0
                ? RepositoryHelper.Query<Patient>().First(w => w.Id == id)
                : new Patient();
            var model = new PatientViewModel();
            model.InitializeFromDb(patient);
            return model;
        }

        /// <summary>
        /// Удалить пациента по идентификатору
        /// </summary>
        /// <param name="id">Идентифкатор пациента</param>
        /// <returns></returns>
        [System.Web.Http.HttpDelete]
        public JsonResult DeletePatient(long id)
        {
            var item = RepositoryHelper.Query<Patient>().First(f => f.Id == id);
            var history = item.VisitHistory.ToList();
            history.ForEach(RepositoryHelper.Delete);
            RepositoryHelper.Delete(item);
            return new JsonResult { Data = Url.Content(@"\PatientCard\Patient")};
        }

        /// <summary>
        /// Добавить или изменить пациента
        /// </summary>
        /// <param name="patient">Модель с данными пациента</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public JsonResult EditPatient(PatientViewModel patient)
        {
            if (patient.Id > 0)
            {
                var item = RepositoryHelper.Query<Patient>().First(f => f.Id == patient.Id);

                item.IIN = patient.IIN;
                item.Address = patient.Address;
                item.PhoneNumber = patient.PhoneNumber;
                item.FIO = patient.FIO;

                RepositoryHelper.Update(item);
            }
            else
            {
                var item = new Patient
                {
                    IIN = patient.IIN,
                    Address = patient.Address,
                    PhoneNumber = patient.PhoneNumber,
                    FIO = patient.FIO
                };

                RepositoryHelper.Insert(item);
            }

            return new JsonResult { Data = Url.Content(@"\PatientCard\Patient") };
        }
    }
}
