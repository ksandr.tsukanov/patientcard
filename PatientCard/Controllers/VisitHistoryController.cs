﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using PatientCard.Helpers;
using PatientCard.Models;
using PatientCard.Models.DataModels;

namespace PatientCard.Controllers
{
    /// <summary>
    /// Информация о посещениях пациентов
    /// </summary>
    public class VisitHistoryController : ApiController
    {
        /// <summary>
        /// Получить данные истории посещений по пациенту
        /// </summary>
        /// <param name="patientId">Идентификатор пациента</param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public List<VisitHistoryViewModel> VisitHistory(long patientId)
        {
            //ViewBag.PatientId = patientId;
            var history = RepositoryHelper.Query<VisitHistory>()
                .Where(w => w.Patient != null && w.Patient.Id == patientId).ToList();
            var model = ViewModelBase.ToViewModel<VisitHistory, VisitHistoryViewModel>(history);

            return model;
        }

        /// <summary>
        /// Получить данные посещения по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор посещения</param>
        /// <param name="patientId">Идентификатор пациента</param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public VisitHistoryViewModel VisitInfo(long id, long patientId)
        {
            var visit = id > 0
                ? RepositoryHelper.Query<VisitHistory>().First(w => w.Id == id)
                : new VisitHistory
                {
                    Patient = RepositoryHelper.Query<Patient>().First(f => f.Id == patientId)
                };
            var model = new VisitHistoryViewModel();
            model.InitializeFromDb(visit);
            return model;
        }

        /// <summary>
        /// Удалить данные о посещении по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор посещения</param>
        /// <returns></returns>
        [System.Web.Http.HttpDelete]
        public ActionResult DeleteVisit(long id)
        {
            var item = RepositoryHelper.Query<VisitHistory>().First(f => f.Id == id);
            var patientId = item.Patient.Id;
            RepositoryHelper.Delete(item);
            return new JsonResult { Data = Url.Content(@"\PatientCard\VisitHistory?patientId=" + patientId) };
        }

        /// <summary>
        /// Добавить или изменить данные о посещении
        /// </summary>
        /// <param name="visit"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public ActionResult EditVisit(VisitHistoryViewModel visit)
        {
            if (visit.Id > 0)
            {
                var item = RepositoryHelper.Query<VisitHistory>().First(f => f.Id == visit.Id);
                item.DoctorType = visit.DoctorType;
                item.Complaints = visit.Complaints;
                item.Diagnosis = visit.Diagnosis;
                item.FIO = visit.FIO;
                item.VisitDate = DateTime.Parse(visit.VisitDate);

                RepositoryHelper.Update(item);
            }
            else
            {
                var item = new VisitHistory
                {
                    DoctorType = visit.DoctorType,
                    Complaints = visit.Complaints,
                    Diagnosis = visit.Diagnosis,
                    FIO = visit.FIO,
                    VisitDate = DateTime.Parse(visit.VisitDate),
                };
                var patient = RepositoryHelper.Query<Patient>().First(f => f.Id == visit.PatientId);
                item.Patient = patient;
                RepositoryHelper.Insert(item);
            }

            return new JsonResult { Data = Url.Content(@"\PatientCard\VisitHistory?patientId=" + visit.PatientId) };
        }

    }
}
