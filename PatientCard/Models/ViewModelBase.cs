﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PatientCard.Models.DataModels;

namespace PatientCard.Models
{
    public class ViewModelBase
    {
        [Key]
        public long Id { get; set; }

        [Required]
        [Display(Name = "ФИО")]
        public string FIO { get; set; }

        public virtual void InitializeFromDb(DataModelBase source)
        {
            Id = source.Id;
            FIO = source.FIO;
        }

        public static List<TTarget> ToViewModel<TSource, TTarget>(List<TSource> source)
            where TSource : DataModelBase
            where TTarget : ViewModelBase, new()
        {
            var target = new List<TTarget>();
            foreach (var sourceItem in source)
            {
                var targetItem = new TTarget();
                targetItem.InitializeFromDb(sourceItem);
                target.Add(targetItem);
            }
            return target;
        }
    }
}