﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace PatientCard.Models.DataModels
{
    public class Enums
    {
        public enum DoctorType
        {

            [Display(Name = "Терапевт")]
            Therapist = 0,

            [Display(Name = "Лор")]
            Lore = 1,

            [Display(Name = "Кардиолог")]
            Cardiologist = 2,

            [Display(Name = "Хирург")]
            Surgeon = 3
        }
    }

    public static class EnumExtensions
    {
        public static string GetDisplayName(this Enums.DoctorType enumValue)
        {
            return enumValue.GetType()
              .GetMember(enumValue.ToString())
              .First()
              .GetCustomAttribute<DisplayAttribute>()
              ?.GetName();
        }
    }
}