﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientCard.Models.DataModels
{
    [Table("patientcard.visithistory")]
    public class VisitHistory : DataModelBase
    {

        [Required]
        public Enums.DoctorType DoctorType { get; set; }

        [Required]
        public string Diagnosis { get; set; }

        [Required]
        public string Complaints { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime VisitDate { get; set; }

        [Required]
        public virtual Patient Patient { get; set; }
    }
}