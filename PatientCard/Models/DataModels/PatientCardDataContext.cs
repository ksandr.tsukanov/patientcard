﻿using System.Configuration;
using System.Data.Common;
using System.Data.Entity;

namespace PatientCard.Models.DataModels
{
    public class PatientCardDataContext : DbContext
    {
        public PatientCardDataContext()
            : base()
        {
            if (bool.Parse(ConfigurationManager.AppSettings["RecreateDb"]))
                Database.SetInitializer(
                    new DropCreateDatabaseIfModelChanges<PatientCardDataContext>());
        }

        public DbSet<Patient> Patient { get; set; }
        public DbSet<VisitHistory> VisitHistory { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Patient>()
                .HasMany(g => g.VisitHistory)
                .WithRequired(s => s.Patient)
                .WillCascadeOnDelete();
        }

        public PatientCardDataContext(DbConnection existingConnection, bool contextOwnsConnection)
                : base(existingConnection, contextOwnsConnection)
        {

        }
    }
}