﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PatientCard.Models.DataModels
{
    public class DataModelBase
    {
        [Key]
        public long Id { get; set; }

        [Required]
        public string FIO { get; set; }
    }
}