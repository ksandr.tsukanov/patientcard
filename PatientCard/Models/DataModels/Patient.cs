﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientCard.Models.DataModels
{

    [Table("patientcard.patient")]
    public class Patient : DataModelBase
    {

        [StringLength(12)]
        [Required]
        public string IIN { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [Phone]
        public string PhoneNumber { get; set; }

        public virtual List<VisitHistory> VisitHistory { get; } = new List<VisitHistory>();
    }
}