﻿using System.ComponentModel.DataAnnotations;
using PatientCard.Models.DataModels;

namespace PatientCard.Models
{
    public class PatientViewModel: ViewModelBase
    {

        [StringLength(12, ErrorMessage = "Длина поля должна быть равна {2}", MinimumLength = 12)]
        [Required]

        [RegularExpression(@"^\d+", ErrorMessage = "ИИН должен содержать только цифры")]
        [Display(Name = "ИИН")]
        public string IIN { get; set; }

        [Required]
        [Display(Name = "Адрес")]
        public string Address { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Номер телефона")]
        [RegularExpression(@"^[8]\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Телефон должен быть в формате 8(###)###-####")]
        public string PhoneNumber { get; set; }

        public override void InitializeFromDb(DataModelBase source)
        {
            base.InitializeFromDb(source);
            var patient = (Patient) source;
            IIN = patient.IIN;
            Address = patient.Address;
            PhoneNumber = patient.PhoneNumber;
        }
    }
}