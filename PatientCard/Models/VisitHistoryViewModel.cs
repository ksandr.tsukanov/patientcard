﻿using System;
using System.ComponentModel.DataAnnotations;
using PatientCard.Models.DataModels;

namespace PatientCard.Models
{
    public class VisitHistoryViewModel : ViewModelBase
    {
        [Required]
        [Display(Name = "Специалист")]
        public Enums.DoctorType DoctorType { get; set; }

        [Display(Name = "Специалист")]
        public string DoctorTypeName => DoctorType.GetDisplayName();

        [Required]
        [Display(Name = "Диагноз")]
        public string Diagnosis { get; set; }

        [Required]
        [Display(Name = "Жалобы")]
        public string Complaints { get; set; }

        [Required]
        [Display(Name = "Дата посещения")]
        public string VisitDate { get; set; }

        [Required]
        public long PatientId { get; set; }

        public override void InitializeFromDb(DataModelBase source)
        {
            base.InitializeFromDb(source);
            var visit = (VisitHistory)source;
            DoctorType = visit.DoctorType;
            Diagnosis = visit.Diagnosis;
            Complaints = visit.Complaints;
            VisitDate = visit.VisitDate.ToString("yyyy-MM-dd");
            PatientId = visit.Patient.Id;
        }
    }
}