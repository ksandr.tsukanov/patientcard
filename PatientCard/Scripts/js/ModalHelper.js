﻿$(function () {
    $("form").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var isValid = form.valid();
        if (!isValid) {
            return;
        }
        var formArray = form.serializeArray();
        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {
            returnArray[formArray[i]['name']] = formArray[i]['value'];
        }
        $.post(this.action, returnArray).done(function (data) {
            window.location.href = data.Data;
        });
    });
})