﻿$(function () {
    $.ajaxSetup({ cache: false });
    $("body").on('click', '.btn-edit', (e) => {
        e.preventDefault();
        var element = this.activeElement;
        $.ajax({ url: element.getAttribute("data-url"), type: 'GET' }).done(function (result) {
            $.post(element.href, result, function (data) {
                $('#dialogContent').html(data);
                $('#dialogContent').find('.modal').modal('show');
            });
        });
    });

    $("body").on('click', '.btn-delete', (e) => {
        e.preventDefault();
        $.ajax({ url: this.activeElement.href, type: 'DELETE' }).done(function (data) {
            window.location.href = data.Data;
        });
    });

    $("#search").on("keyup", function () {
        var value = $(this).val();
        $("table tr").each(function (index) {
            if (index !== 0) {
                $row = $(this);
                var text = $row.find("td.filtering").text();
                if (!text.includes(value))
                    $row.hide();
                else
                    $row.show();
            }
        });
    });
})

function sortByKeyDesc(array, key) {
    return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
}
function sortByKeyAsc(array, key) {
    return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

function GetSortParameters(url) {
    var qs = url.substring(url.indexOf('?') + 1).split('&');
    var result = {};
    for (var i = 0; i < qs.length; i++) {
        qs[i] = qs[i].split('=');
        result[qs[i][0]] = decodeURIComponent(qs[i][1]);
    }
    return result;
}